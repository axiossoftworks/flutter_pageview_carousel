library flutter_pageview_carousel;

import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:photo_view/photo_view.dart';

class Carousel extends StatefulWidget {
  final List images;
  final double height;
  final double width;
  final Color color;
  Carousel({Key key, this.height, this.width, this.color, this.images})
      : super(key: key);
  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: 0, viewportFraction: 1);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        height: widget.height ??
            (1 / MediaQuery.of(context).size.aspectRatio) * 120,
        margin: EdgeInsets.only(top: 8, bottom: 8),
        width: widget.width ?? MediaQuery.of(context).size.width,
        child: Center(
          child: PageIndicatorContainer(
            child: PageView.builder(
              controller: pageController,
              itemCount: widget.images.length,
              itemBuilder: (context, position) {
                return imageSlider(position, widget.color ?? Colors.white);
              },
            ),
            align: IndicatorAlign.bottom,
            length: widget.images.length,
            indicatorColor: Colors.grey[50],
            indicatorSelectorColor: widget.color ?? Colors.blue[400],
          ),
        ),
      ),

      // Positioned(
      //   top: MediaQuery.of(context).size.height * .15,
      //   left: 0,
      //   child: GestureDetector(
      //     onTap: () {
      //       pageController.previousPage(
      //           curve: Curves.easeInCirc,
      //           duration: Duration(milliseconds: 400));
      //     },
      //     child: Card(
      //       color: Colors.transparent,
      //       child: Icon(
      //         Icons.arrow_left,
      //         color: Colors.white,
      //         size: 40,
      //       ),
      //     ),
      //   ),
      // ),
      // Positioned(
      //   top: MediaQuery.of(context).size.height * .15,
      //   right: 0,
      //   child: GestureDetector(
      //     onTap: () {
      //       pageController.nextPage(
      //           curve: Curves.easeInOutCirc,
      //           duration: Duration(milliseconds: 400));
      //     },
      //     child: Card(
      //       color: Colors.transparent,
      //       child: Icon(
      //         Icons.arrow_right,
      //         color: Colors.white,
      //         size: 40,
      //       ),
      //     ),
      //   ),
      // )
    ]);
  }

  imageSlider(int position, color) {
    return AnimatedBuilder(
      animation: pageController,
      builder: (context, widget) {
        return SizedBox(
          child: widget,
        );
      },
      child: Ink(
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    ImageViewer(widget.images[position], color)));
          },
          child: Image.network(
            widget.images[position],
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}

class ImageViewer extends StatelessWidget {
  final String image;
  final Color color;
  ImageViewer(this.image, this.color);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: PhotoView(
                backgroundDecoration: BoxDecoration(color: Colors.transparent),
                imageProvider: NetworkImage(image, scale: 1.2),
              ),
            ),
            Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 8.0),
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: color,
                    ),
                    splashColor: color,
                    onPressed: () => Navigator.pop(context),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
