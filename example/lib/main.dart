import 'package:flutter/material.dart';
import 'package:flutter_pageview_carousel/flutter_pageview_carousel.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: ImageCarouselWidget(),
    );
  }
}

class ImageCarouselWidget extends StatefulWidget {
  @override
  _ImageCarouselWidgetState createState() => _ImageCarouselWidgetState();
}

class _ImageCarouselWidgetState extends State<ImageCarouselWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Carousel(
        images: [
          'https://dabalimart.com/uploads/thumb_202002280807202002161022fantech_1.jpg',
          'https://dabalimart.com/uploads/thumb_202002280807oneplus-7t-2_copy.jpg',
          'https://dabalimart.com/uploads/thumb_202002280807Samsung-Galaxy-A30s_copy.jpg'
        ],
      ),
    );
  }
}
